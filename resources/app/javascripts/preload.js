'use strict'
var ipcRenderer = require('electron').ipcRenderer
require('andapp-shellapp-sdk').renderer.preload()
var path = require('path')
var codeCache = require('../javascripts/CodeCache')
var GranblueLocations = require('../javascripts/granbluelocations')
global.AndAppClient = {
  appUpdate: function () {
    ipcRenderer.sendToHost('appUpdate')
  },
  toggleWindow: function (isOpen) {
    var zoom = document.querySelector('html').style.zoom
    ipcRenderer.sendToHost('onToggleSubmenu', { isOpen: isOpen, zoom: zoom })
  },
  hideBackground: function () {
    ipcRenderer.sendToHost('hideBackground')
  },
  setLanguage: function (lang) {
    ipcRenderer.sendToHost('setLanguage', lang)
  },
  didLoadCjs: function () {
    ipcRenderer.sendToHost('didLoadCjs')
  }
}
window.addEventListener('DOMContentLoaded', function () {
  document.ondragover = document.ondrop = document.ondragstart = function (event) {
    event.preventDefault()
    return false
  }
  document.onmousedown = function (event) {
    if (typeof event.buttons !== 'undefined' && event.buttons === 8) {
      window.history.back()
    }
  }
  var zoom = document.querySelector('html').style.zoom
  if (!GranblueLocations.isValid(location.href)) {
    ipcRenderer.sendToHost('initWindow', {
      status: {
        isOpen: false,
        isNoSubmenu: true
      },
      zoom: zoom
    })
    return
  }
  var pcsbm = JSON.parse(window.localStorage.getItem('gbf_pcsbm'))
  var isOpen = !!pcsbm && pcsbm.is_open
  var isNoSubmenu = document.getElementById('submenu') === null
  ipcRenderer.sendToHost('initWindow', {
    status: {
      isOpen: isOpen,
      isNoSubmenu: isNoSubmenu },
    zoom: zoom
  })
})
ipcRenderer.on('overrideFontFamily', function () {
  var pageBody = document.getElementsByTagName('body')[0]
  var pageFontFamily = getComputedStyle(pageBody, '').fontFamily
  var targetFontName = '"FOT-ニューロダン Pro M", '
  var backupFontName = '"FOT-テロップ明朝 Pro D",'
  if (pageFontFamily.indexOf(targetFontName) === 0) {
    pageFontFamily = pageFontFamily.split(targetFontName)[1]
  } else if (pageFontFamily.indexOf(backupFontName) === 0) {
    pageFontFamily = pageFontFamily.split(backupFontName)[1]
  }
  pageBody.style.fontFamily = pageFontFamily
})

ipcRenderer.on('setBodyPadding', function (event, value) {
  if (!GranblueLocations.isValid(location.href)) {
    var pageBody = document.getElementsByTagName('body')[0]
    pageBody.style.paddingBottom = value
  }
})

ipcRenderer.on('message', function (event, message, targetOrigin) {
  message = message || {}
  window.postMessage(message, targetOrigin)
})
