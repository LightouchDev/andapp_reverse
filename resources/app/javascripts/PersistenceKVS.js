'use strict'

const app = require('electron').app
const fs = require('fs')
const path = require('path')

var promisifyMethods = ['write', 'open', 'readFile', 'stat', 'fsync', 'close']
var pfs = {}
promisifyMethods.forEach(function (method) {
  pfs[method] = promisifyFs(fs[method])
})

function promisifyFs (callback) {
  return function () {
    for (var args = [], index = 0; index < arguments.length; index++) {
      args[index] = arguments[index]
    }
    return new Promise(function (resolve, reject) {
      try {
        callback.apply(
          void 0,
          args.concat([
            function (a) {
              for (var args = [], index = 1; index < arguments.length; index++) {
                args[index - 1] = arguments[index]
              }
              return a ? void reject(a) : void resolve.apply(void 0, args)
            }
          ])
        )
      } catch (error) {
        reject(error)
      }
    })
  }
}

function PersistenceKVS (filename) {
  var self = this
  this._filePath = path.join(app.getPath('userData'), filename)
  this._config = null
  this._loadedPromise = new Promise(function (resolve, reject) {
    self._loadResolve = resolve
    self._loadReject = reject
  })

  this.loaded = this._loadedPromise

  /*
  Object.defineProperty(self.prototype, 'loaded', {
    get () {
      return self._loadedPromise
    },
    enumerable: true,
    configurable: true
  })
  */

  this.get = function (prop) {
    return this._config[prop]
  }

  this.set = function (prop, value) {
    value === undefined
      ? delete this._config[prop]
      : (this._config[prop] = value)
    this._fsyncWrite(JSON.stringify(this._config))['catch'](function () {})
  }

  // create / load file to config when load
  this.load = function () {
    var self = this
    this._exists()
      .then(function (result) {
        return result
          ? pfs.readFile(self._filePath).then(function (result) {
            try {
              self._config = JSON.parse(String(result))
              return undefined
            } catch (error) {
              return self._initFile()
            }
          })
          : self._initFile()
      })
      .then(function () {
        self._loadResolve()
      })
      ['catch'](function (b) {
        self._loadReject(b)
      })
  }

  this._initFile = function () {
    var self = this
    return this._fsyncWrite('{}').then(function () {
      self._config = {}
    })
  }

  // Check specific file exist and pass to handler
  this._exists = function (callback) {
    return pfs
      .stat(this._filePath)
      .then(function (result) {
        return Boolean(result)
      })
      ['catch'](function () {
        return false
      })
  }

  this._fsyncWrite = function (content) {
    var self = this
    return new Promise(function (resolve, reject) {
      var fd = null
      pfs
        .open(self._filePath, 'w')
        .then(function (result) {
          fd = result
          return pfs.write(fd, content)
        })
        .then(function () {
          return pfs.fsync(fd)
        })
        .then(function () {
          pfs.close(fd)
          resolve()
        })
        ['catch'](function (error) {
          if (fd) {
            pfs.close(fd)
          }
          reject(error)
        })
    })
  }
}

module.exports = PersistenceKVS
