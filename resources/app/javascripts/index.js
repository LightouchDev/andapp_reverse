/* globals timerResize */
/* eslint-disable no-global-assign */
var electron = require('electron')
var remote = electron.remote
var ipcRenderer = electron.ipcRenderer
var shell = electron.shell
var path = require('path')
var andAppSDKRenderer = require('andapp-shellapp-sdk').renderer
var codeCache = require('./javascripts/CodeCache')
var gbfConfig = remote.getGlobal('gbfConfig')

var debugBuild = false
const unknownVar2 = debugBuild && debugBuild[0]
const unknownVar3 = debugBuild && debugBuild[1]
const debugMode = debugBuild && debugBuild[3]
window.timerResize = false
window.bW = window.bH = null
var baseUrl = remote.getGlobal('baseUrl')
const BASE_URL = unknownVar3 && baseUrl ? baseUrl : 'http://gbf.game.mbga.jp/'
var WIN_MAX_WIDTH_CLOSE,
  WIN_MAX_WIDTH_OPEN,
  WIN_MAX_WIDTH_OTHER,
  WIN_MIN_WIDTH_CLOSE,
  WIN_MIN_WIDTH_OPEN,
  WIN_MIN_WIDTH_OTHER,
  WIN_MAX_HEIGHT,
  WIN_MIN_HEIGHT,
  WINDOW_MARGIN_W
const WIDTH_GAME_BASE = 320
const WIDTH_SUBMENU_BUTTON = 64
const WIDTH_OPEN_SUBMENU = 704
const WIDTH_CLOSE_SUBMENU = 384
const BASIC_MARGIN_H = 100
const BASIC_MARGIN_W = 16
const NAVIGATE_INTERVAL = 200
var webView = null

function init () {
  var browserWindow = remote.getCurrentWindow()
  var contentBounds = browserWindow.getContentBounds()
  function clearLock () {
    clearTimeout(window.lock)
    window.lock = setTimeout(function () {
      clearTimeout(window.lock)
      delete window.lock
    }, NAVIGATE_INTERVAL)
  }
  function isLocked () {
    return !!window.lock
  }
  function calcContentBounds (size) {
    var contentBounds = browserWindow.getContentBounds()
    contentBounds.height = parseInt(size.height, 10)
    contentBounds.width = parseInt(size.width, 10)
    return {
      x: contentBounds.x,
      y: contentBounds.y,
      width: contentBounds.width,
      height: contentBounds.height
    }
  }
  window.bW = contentBounds.width
  window.bH = contentBounds.height
  webView.style.height = contentBounds.height - 10 + 'px'
  WINDOW_MARGIN_W = browserWindow.getSize()[0] - browserWindow.getContentSize()[0]
  var sizePreset = {
    size: [
      { postMsg: 'sizeS_close', bounds: { width: 400, height: 579 } },
      { postMsg: 'sizeS_open', bounds: { width: 720, height: 579 } },
      { postMsg: 'sizeS_other', bounds: { width: 336, height: 579 } },
      {
        postMsg: 'sizeM_close',
        bounds: {
          width: 592,
          height:
            screen.height > 961 ? 861 : screen.height - BASIC_MARGIN_H + 10
        }
      },
      {
        postMsg: 'sizeM_open',
        bounds: {
          width: 1072,
          height:
            screen.height > 961 ? 861 : screen.height - BASIC_MARGIN_H + 10
        }
      },
      {
        postMsg: 'sizeM_other',
        bounds: {
          width: 496,
          height:
            screen.height > 961 ? 861 : screen.height - BASIC_MARGIN_H + 10
        }
      },
      {
        postMsg: 'sizeL',
        bounds: {
          width: 784,
          height:
            screen.height > 1236 ? 1136 : screen.height - BASIC_MARGIN_H + 10
        }
      },
      {
        postMsg: 'sizeL_open',
        bounds: {
          width: 1424,
          height:
            screen.height > 1236 ? 1136 : screen.height - BASIC_MARGIN_H + 10
        }
      },
      {
        postMsg: 'sizeL_other',
        bounds: {
          width: 656,
          height:
            screen.height > 1236 ? 1136 : screen.height - BASIC_MARGIN_H + 10
        }
      }
    ]
  }
  var maximumWindowSize = browserWindow.getMaximumSize()
  WIN_MAX_WIDTH_CLOSE = maximumWindowSize[0]
  var maximumContentWidth = WIN_MAX_WIDTH_CLOSE - WINDOW_MARGIN_W - BASIC_MARGIN_W
  WIN_MAX_WIDTH_OPEN =
    Math.round(WIDTH_OPEN_SUBMENU * maximumContentWidth / WIDTH_CLOSE_SUBMENU) +
    BASIC_MARGIN_W + WINDOW_MARGIN_W
  WIN_MAX_WIDTH_OTHER =
    2 * (maximumContentWidth / 2 - WIDTH_SUBMENU_BUTTON) + BASIC_MARGIN_W + WINDOW_MARGIN_W
  var minimumWindowSize = browserWindow.getMinimumSize()
  WIN_MIN_WIDTH_CLOSE = minimumWindowSize[0]
  var minimumContentWidth = WIN_MIN_WIDTH_CLOSE - WINDOW_MARGIN_W - BASIC_MARGIN_W
  WIN_MIN_WIDTH_OPEN =
    Math.round(WIDTH_OPEN_SUBMENU * minimumContentWidth / WIDTH_CLOSE_SUBMENU) +
    BASIC_MARGIN_W + WINDOW_MARGIN_W
  WIN_MIN_WIDTH_OTHER =
    WIN_MIN_WIDTH_CLOSE - WIDTH_SUBMENU_BUTTON
  WIN_MAX_HEIGHT = screen.height
  WIN_MIN_HEIGHT = minimumWindowSize[1]
  var classOn = function (event) {
    event.target.className = event.target.className.replace('off', 'on')
  }
  var classOff = function (event) {
    event.target.className = event.target.className.replace('on', 'off')
  }
  var footerButton = ['reload', 'mypage', 'back']
  footerButton.forEach(function (button) {
    $('#footer-' + button).addEventListener('mouseover', classOn)
    $('#footer-' + button).addEventListener('mouseout', classOff)
  })
  var sizeButton = ['s', 'm', 'l']
  sizeButton.forEach(function (resizer) {
    $('#btn-resize-' + resizer).addEventListener('mouseover', classOn)
    $('#btn-resize-' + resizer).addEventListener('mouseout', classOff)
  })
  $('#footer-back').addEventListener('click', function () {
    if (!isLocked()) {
      webView.goBack()
      clearLock()
    }
  })
  $('#footer-reload').addEventListener('click', function () {
    if (!isLocked()) {
      window.debugMode === true
        ? webView.reloadIgnoringCache()
        : webView.reload()
      clearLock()
    }
  })
  $('#footer-mypage').addEventListener('click', function () {
    if (!isLocked()) {
      webView.loadURL(BASE_URL + '?_=' + new Date().getTime() + '#mypage')
      clearLock()
    }
  })
  $('#btn-resize-s').addEventListener('click', function () {
    var preset = window.isNoSubmenu === true
      ? 2
      : window.isOpenSubmenu === true
        ? 1
        : 0
    browserWindow.setContentBounds(calcContentBounds(sizePreset.size[preset].bounds))
  })
  $('#btn-resize-m').addEventListener('click', function () {
    var preset = window.isNoSubmenu === true
      ? 5
      : window.isOpenSubmenu === true
        ? 4
        : 3
    browserWindow.setContentBounds(calcContentBounds(sizePreset.size[preset].bounds))
  })
  $('#btn-resize-l').addEventListener('click', function () {
    var preset = window.isNoSubmenu === true
      ? 8
      : window.isOpenSubmenu === true
        ? 7
        : 6
    browserWindow.setContentBounds(calcContentBounds(sizePreset.size[preset].bounds))
  })
  window.shouldReload = true
  window.isNoSubmenu = true
  initFooter()
  if (gbfConfig.get('lang') === 'en') {
    $('#footer-mypage').classList.add('en')
  }
  document.ondragover = document.ondrop = document.ondragstart = function (event) {
    event.preventDefault()
    return false
  }
  browserWindow.on('resize', onBoundsChanged)
  browserWindow.on('minimize', onMinimize)
  browserWindow.on('restore', onRestore)
  browserWindow.on('enter-html-full-screen', enterFullScreen)
  browserWindow.on('leave-html-full-screen', leaveFullScreen)
  wvInit.call(this, webView, 'sizeS')
}
function onMinimize () {
  window.isMinimized = true
}
function onRestore () {
  window.isMinimized = false
}
function enterFullScreen () {
  $('#footer').setAttribute('style', 'display:none;')
  if (!remote.getGlobal('isMac')) {
    window.isFullScreen = true
    window.tmpHeight = webView.style.height
    webView.setAttribute('style', 'height:' + screen.availHeight + 'px;')
  }
}
function leaveFullScreen () {
  $('#footer').setAttribute('style', 'display:block;')
  if (!remote.getGlobal('isMac')) {
    window.isFullScreen = false
    window.tmpHeight = window.tmpHeight || screen.availHeight / 2 + 'px'
    webView.setAttribute('style', 'height:' + window.tmpHeight + ';')
  }
  initFooter()
  onBoundsChanged()
}
function executeScript (webContents, filename) {
  var filePath = path.join(__dirname, filename)
  codeCache.load(filePath, function (error, content) {
    error || webContents.executeJavaScript(content, false)
  })
}
function insertCSS (webContents, filename) {
  var filePath = path.join(__dirname, filename)
  codeCache.load(filePath, function (error, content) {
    error || webContents.insertCSS(content)
  })
}
function postMessage (webContents, message, targetOrigin) {
  webContents.send('message', message, targetOrigin)
}
function initFooter () {
  var zoom = 1
  if (window.footerZoom) zoom = window.footerZoom
  else {
    var contentWidth
    if (window.isNoSubmenu) {
      contentWidth = WIDTH_GAME_BASE
    } else if (window.isOpenSubmenu) {
      contentWidth = WIDTH_OPEN_SUBMENU
    } else {
      contentWidth = WIDTH_CLOSE_SUBMENU
    }
    zoom = (window.bW - BASIC_MARGIN_W) / contentWidth
  }
  $('#footer').setAttribute('style', 'zoom:' + zoom + ';')
}
function setLanguage (newLang) {
  var lang = gbfConfig.get('lang')
  if (lang !== newLang) {
    newLang === 'en'
      ? $('#footer-mypage').classList.add('en')
      : $('#footer-mypage').classList.remove('en')
    gbfConfig.set('lang', newLang)
  }
}

window.onload = function () {
  webView = $('#webview-game')
  var userAgent = 'Mozilla/5.0 (Linux; Android 4.4.4; 401SO Build/23.0.H.0.302) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/34.0.0.0 Mobile Safari/537.36 GbfAA/1.0'

  andAppSDKRenderer.init(webView, { overrideUAString: userAgent }, function (error) {
    if (error) {
      alert(error.meaage)
      remote.app.quit()
    }
    webView.preload = 'javascripts/preload.js'
    webView.src = BASE_URL
    init()
  })
  /* Custom modify demo Begin */
  document.title += ' Reverse Engineering Edition'
  // Custom hotkey
  const os = require('os')
  window.onkeydown = event => {
    if (os.platform === 'darwin') {
      // Option + Alt + I: open game view DevTools on OSX
      if (event.metaKey && event.altKey && event.code === 'KeyI') {
        webView.openDevTools({mode: 'detach'})
      }
    } else {
      // F12: open game view DevTools
      if (!event.ctrlKey && !event.altKey && !event.metaKey && event.code === 'F12') {
        webView.openDevTools({mode: 'detach'})
      }
    }

    // Ctrl + Alt + I: open host view DevTools
    if (event.ctrlKey && event.altKey && event.code === 'KeyI') {
      remote.getCurrentWebContents().openDevTools({mode: 'detach'})
    }
  }
  /* Custom modify demo End */
}
var $ = function (query) {
  return document.querySelector(query)
}
var setZoomLimit = function () {
  if (remote.getGlobal('isMac') !== !1) {
    electron.webFrame.setZoomLevelLimits(1, 1)
  }
}
var wvInit = function (webview, command) {
  function checkWidthAndInitFooter (maxWidth, minWidth) {
    var currentWindow = remote.getCurrentWindow()
    var contentBounds = currentWindow.getContentBounds()
    var currentWidth = contentBounds.width + BASIC_MARGIN_W
    if (minWidth > currentWidth) {
      var windowRect = {
        x: contentBounds.x,
        y: contentBounds.y,
        width: minWidth,
        height: contentBounds.height
      }
      window.shouldReload = true
      currentWindow.setContentBounds(windowRect)
    } else if (currentWidth > maxWidth) {
      var windowRect = {
        x: contentBounds.x,
        y: contentBounds.y,
        width: maxWidth,
        height: contentBounds.height
      }
      window.shouldReload = true
      currentWindow.setContentBounds(windowRect)
    }
    initFooter()
  }

  function setSubMenuState (state) {
    var subMenuState = {
      isOpenSubmenu: window.isOpenSubmenu,
      isNoSubmenu: window.isNoSubmenu
    }

    window.isOpenSubmenu = state.isOpen
    window.isNoSubmenu = state.isNoSubmenu
    var currentWindow = remote.getCurrentWindow()
    if (typeof subMenuState.isOpenSubmenu === 'undefined') {
      window.shouldReload = true
      if (window.isNoSubmenu === true) {
        currentWindow.setMaximumSize(WIN_MAX_WIDTH_OTHER, WIN_MAX_HEIGHT)
        currentWindow.setMinimumSize(WIN_MIN_WIDTH_OTHER, WIN_MIN_HEIGHT)
        checkWidthAndInitFooter(WIN_MAX_WIDTH_OTHER, WIN_MIN_WIDTH_OTHER)
      } else {
        if (window.isOpenSubmenu === true) {
          currentWindow.setMaximumSize(WIN_MAX_WIDTH_OPEN, WIN_MAX_HEIGHT)
          currentWindow.setMinimumSize(WIN_MIN_WIDTH_OPEN, WIN_MIN_HEIGHT)
          checkWidthAndInitFooter(WIN_MAX_WIDTH_OPEN, WIN_MIN_WIDTH_OPEN)
        } else {
          checkWidthAndInitFooter(WIN_MAX_WIDTH_CLOSE, WIN_MIN_WIDTH_CLOSE)
        }
      }
    } else if (window.isOpenSubmenu !== subMenuState.isOpenSubmenu) {
      window.isOpenSubmenu === true ? openSubMenu() : closeSubMenu()
    } else if (window.isNoSubmenu === true) {
      currentWindow.setMaximumSize(WIN_MAX_WIDTH_OTHER, WIN_MAX_HEIGHT)
      currentWindow.setMinimumSize(WIN_MIN_WIDTH_OTHER, WIN_MIN_HEIGHT)
      checkWidthAndInitFooter(WIN_MAX_WIDTH_OTHER, WIN_MIN_WIDTH_OTHER)
    } else if (window.isOpenSubmenu === true) {
      currentWindow.setMaximumSize(WIN_MAX_WIDTH_OPEN, WIN_MAX_HEIGHT)
      currentWindow.setMinimumSize(WIN_MIN_WIDTH_OPEN, WIN_MIN_HEIGHT)
      checkWidthAndInitFooter(WIN_MAX_WIDTH_OPEN, WIN_MIN_WIDTH_OPEN)
    } else {
      currentWindow.setMaximumSize(WIN_MAX_WIDTH_CLOSE, WIN_MAX_HEIGHT)
      currentWindow.setMinimumSize(WIN_MIN_WIDTH_CLOSE, WIN_MIN_HEIGHT)
      checkWidthAndInitFooter(WIN_MAX_WIDTH_CLOSE, WIN_MIN_WIDTH_CLOSE)
    }
    window.isNoSubmenu === true
      ? $('#appVer').classList.remove('on-sub')
      : $('#appVer').classList.add('on-sub')
  }

  function openSubMenu () {
    window.isOpenSubmenu = true
    var currentWindow = remote.getCurrentWindow()
    var contentBounds = currentWindow.getContentBounds()
    var contentWidth = contentBounds.width - BASIC_MARGIN_W
    var windowWidth =
      Math.round(contentWidth * WIDTH_OPEN_SUBMENU / WIDTH_CLOSE_SUBMENU) +
      BASIC_MARGIN_W

    window.shouldReload = false
    currentWindow.setMaximumSize(WIN_MAX_WIDTH_OPEN, WIN_MAX_HEIGHT)
    currentWindow.setMinimumSize(WIN_MIN_WIDTH_OPEN, WIN_MIN_HEIGHT)
    if (windowWidth > (WIN_MAX_WIDTH_OPEN - WINDOW_MARGIN_W)) {
      window.shouldReload = true
      windowWidth = WIN_MAX_WIDTH_OPEN - WINDOW_MARGIN_W
    }
    var windowRect = {
      x: contentBounds.x,
      y: contentBounds.y,
      width: windowWidth,
      height: contentBounds.height
    }
    currentWindow.setContentBounds(windowRect)
  }
  function closeSubMenu () {
    window.isOpenSubmenu = false
    var currentWindow = remote.getCurrentWindow()
    var contentBounds = currentWindow.getContentBounds()
    var contentWidth = contentBounds.width - BASIC_MARGIN_W
    var windowWidth =
        Math.round(contentWidth * WIDTH_CLOSE_SUBMENU / WIDTH_OPEN_SUBMENU) +
        BASIC_MARGIN_W
    window.shouldReload = false
    currentWindow.setMaximumSize(WIN_MAX_WIDTH_CLOSE, WIN_MAX_HEIGHT)
    currentWindow.setMinimumSize(WIN_MIN_WIDTH_CLOSE, WIN_MIN_HEIGHT)
    if (windowWidth < (WIN_MIN_WIDTH_CLOSE - WINDOW_MARGIN_W)) {
      window.shouldReload = true
      windowWidth = WIN_MIN_WIDTH_CLOSE - WINDOW_MARGIN_W
    }
    var windowRect = {
      x: contentBounds.x,
      y: contentBounds.y,
      width: windowWidth,
      height: contentBounds.height
    }
    currentWindow.setContentBounds(windowRect)
  }
  webview.addEventListener('dom-ready', function () {
    $('#footer').classList.remove('hide')
    remote.getGlobal('isMac') === true && webview.send('overrideFontFamily')
  })
  webview.addEventListener('did-finish-load', function () {
    executeScript(webview, 'javascripts/create-touch.js')
    executeScript(webview, 'javascripts/app-override.js')
    insertCSS(webview, 'stylesheets/override.css')
    postMessage(webview, { command: command }, '*')
    if (debugMode) webview.openDevTools()
  })
  webview.addEventListener('new-window', function (event) {
    /^https?:\/\/(?:[^./]+\.)*mbga\.jp\//.test(event.url)
      ? webview.loadURL(event.url)
      : shell.openExternal(event.url)
  })
  webview.addEventListener('ipc-message', function (event) {
    switch (event.channel) {
      case 'onToggleSubmenu':
        window.footerZoom = event.args[0].zoom
        event.args[0].isOpen === true ? openSubMenu() : closeSubMenu()
        break
      case 'initWindow':
        window.footerZoom = event.args[0].zoom
        setSubMenuState(event.args[0].status)
        break
      case 'appUpdate':
        ipcRenderer.send('appUpdate')
        break
      case 'hideBackground':
        $('#footer').classList.add('hide')
        break
      case 'setLanguage':
        setLanguage(event.args[0])
        break
      case 'didLoadCjs':
        setZoomLimit()
    }
  })
  var version = !debugBuild ? remote.getGlobal('appVersionName') : 'debug'
  $('#appVer').innerHTML = 'Ver. ' + version
}
var onBoundsChanged = function () {
  if (timerResize !== false) clearTimeout(timerResize)
  var timeoutPeriod = window.shouldReload === true ? 200 : 0
  timerResize = setTimeout(function () {
    if (window.isMinimized !== true && window.isFullScreen !== true) {
      WIN_MAX_HEIGHT = screen.height
      var contentBounds = remote.getCurrentWindow().getContentBounds()
      var width = contentBounds.width
      var height = contentBounds.height
      if (window.bW !== width || window.bH !== height) {
        window.bW = width
        window.bH = height
        ipcRenderer.send('onBoundsChanged', {
          width: window.bW,
          height: window.bH
        })
        var webview = $('#webview-game')
        initFooter()
        webview.style.height = height - 10 + 'px'
        if (window.shouldReload === true) {
          webview.reload()
        }
        window.shouldReload = true
      }
    }
  }, timeoutPeriod)
}
