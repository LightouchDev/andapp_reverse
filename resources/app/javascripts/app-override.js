!(function () {
  var gameContents = document.querySelector('.contents')
  var gameContentsObserver = new MutationObserver(function (a) {
    gameContents.forEach(function (a) {
      var className = a.target.className
      var length = a.addedNodes.length
      if (length && className === 'contents') removeTargetAttr.call()
    })
  })
  var config = { childList: true, subtree: true }
  var removeTargetAttr = function () {
    $("[target='_blank']").removeAttr('target')
  }
  gameContentsObserver.observe(gameContents, config)
  window.addEventListener('message', function (a) {
    switch (a.data.command) {
      case 'sizeS':
        break
      case 'sizeM':
        break
      case 'sizeL':
    }
  }, false)
})()
