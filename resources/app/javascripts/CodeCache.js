'use strict'
const fs = require('fs')

function CodeCache () {
  this.codeCache = {}
  this.load = function (filePath, callback) {
    if (this.codeCache[filePath]) {
      callback(null, this.codeCache[filePath])
      return
    }
    try {
      fs.readFile(filePath, function (error, result) {
        if (!error) {
          this.codeCache[filePath] = result.toString()
        }
        callback(null, this.codeCache[filePath])
      }.bind(this))
    } catch (error) {
      callback(error)
    }
  }
}
module.exports = new CodeCache()
