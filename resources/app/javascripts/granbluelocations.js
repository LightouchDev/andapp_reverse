var GranblueLocations = (function () {
  var whiteList
  function a () {
    whiteList = ['.*gbf.game.mbga.jp']
  }
  a.isValid = function (url) {
    a()
    var urlParser = new RegExp('^(https?|http)://([^/]*)/([-a-z\\d%/_.~+]*)*(.*)', 'i')
    var matches = urlParser.exec(url)
    if (matches === null) return false
    var domain = matches[2]
    for (var prop in whiteList) {
      if (whiteList.hasOwnProperty(prop)) {
        var whiteDomain = new RegExp(whiteList[prop], 'i')
        if (whiteDomain.test(domain)) return true
      }
    }
    return false
  }
  return a
})()
module.exports = GranblueLocations
