function popup (headerText, messageText) {
  var popup = document.getElementById('popup')
  if (popup.innerHTML === '') {
    var HTMLContent =
      '<div class="prt-popup-header">' +
      headerText +
      '</div><div class="prt-popup-body"><div class="prt-popup-info"><div class="txt-popup-message">' +
      messageText +
      '</div></div></div><div class="prt-popup-footer"><div class="button-blue-xxl"></div></div>'
    popup.innerHTML = HTMLContent
  } else {
    var headerEl = popup.querySelector('.prt-popup-header')
    var messageEl = popup.querySelector('.txt-popup-message')
    if (headerEl) headerEl.innerHTML = headerText
    if (messageEl) messageEl.innerHTML = messageText
  }
  var body = document.body
  var left = body.offsetWidth / 2 - 160
  document
    .getElementById('protect')
    .setAttribute('style', 'display: block;')
  popup.setAttribute(
    'style',
    'display: block; top: 40%; left: ' + left + 'px;'
  )
  popup.className = 'pop-usual popup-in'
  var buttonOn = function (elem) {
    elem.currentTarget.className = 'button-blue-xxl on'
  }
  var button = document.querySelector('.button-blue-xxl')
  button.addEventListener('mousedown', buttonOn)
  button.addEventListener('mouseup', function (elem) {
    elem.currentTarget.className = 'button-blue-xxl'
    popup.className = 'pop-usual popup-out'
    elem.currentTarget.removeEventListener('mousedown', buttonOn, false)

    // remove this event listener
    elem.currentTarget.removeEventListener('mouseup', arguments.callee, false)

    setTimeout(function () {
      popup.setAttribute('style', 'display: none;')
      document.getElementById('protect').setAttribute('style', 'display: none;')
    }, 500)
  })
}
module.exports = popup
