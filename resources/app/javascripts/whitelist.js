const WHITE_DOMAIN = [
  '*gbf.game.mbga.jp',
  '203.104.248.16:13450',
  'cfg.smt.docomo.ne.jp',
  'connect.auone.jp',
  'id.my.softbank.jp',
  'www.webmoney.ne.jp',
  'acs.cafis-paynet.jp',
  'i.mydocomo.com',
  'payment1.smt.docomo.ne.jp',
  'mobage.sc.omtrdc.net',
  'po.id.my.softbank.jp',
  'mobage8.tt.omtrdc.net',
  'j.amoad.com',
  'app-adforce.jp',
  'www.googleadservices.com',
  's2.nend.net',
  's.nend.net',
  'spdmg.i-mobile.co.jp',
  'spdeliver.i-mobile.co.jp',
  'spdmg-backend.i-mobile.co.jp',
  'connect.facebook.net',
  'www.facebook.com',
  'public.astrsk.net',
  'd-track.send.microad.jp',
  'ssend.microad.jp',
  'd-cache.microad.jp',
  'accounts.google.com',
  'accounts.youtube.com',
  'plus.google.com',
  'www.googleapis.com',
  'm.facebook.com',
  'beyondthesky.granbluefantasy.jp',
  'www.famitsu.com',
  's.famitsu.com',
  'granbluefantasy.jp',
  'support.google.com',
  'orchestra.granbluefantasy.jp',
  'va.pia.jp',
  'pia.jp',
  'w.pia.jp',
  'granbluefantasy-cp.jp',
  'campaign.granbluefantasy.jp'
]
var whiteListRegExp = []
for (var i = 0; i < WHITE_DOMAIN.length; i++) {
  var url = '^(https?|http)://' + WHITE_DOMAIN[i] + '/([-a-z\\d%/_.~+]*)*(.*)'
  whiteListRegExp.push(new RegExp(url, 'i'))
}
whiteListRegExp.push(
  new RegExp('^https?:\\/\\/(?:[^./]+\\.)*rakuten\\.co\\.jp\\/')
)

module.exports = whiteListRegExp
