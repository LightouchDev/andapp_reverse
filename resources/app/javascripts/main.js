'use strict'

const crypto = require('crypto')
const { app, BrowserWindow, dialog, ipcMain, shell, Menu } = require('electron')
const shellAppSDKMain = require('andapp-shellapp-sdk').main

const gbfConfig = new (require('./PersistenceKVS'))('gbf-config.json')
const WhiteList = require('./whitelist.js')

let mainWindow = null
let windowBounds

function getRandomToken () {
  return crypto.randomBytes(32).toString('hex')
}

function getErrorMessageForDisplay (error) {
  const validCodes = [-30101, -30201]
  validCodes.some(function (code) {
    return error.code === code
  })
}

function _displayMessage (content, callback) {
  var options = {
    type: content.type || 'info',
    buttons: content.buttons || ['OK'],
    message: content.message,
    detail: content.detail
  }
  dialog.showMessageBox(options, callback)
}

function displayErrorAndQuit (error) {
  _displayMessage({
    type: 'error',
    message: error.code,
    detail: error.message
  },
  function () {
    app.quit()
  }
  )
}
function onClose () {
  gbfConfig.set('width', windowBounds.width)
  gbfConfig.set('height', windowBounds.height)
}
function onClosed () {
  mainWindow = null
  app.quit()
}

const shouldQuit = app.makeSingleInstance(function (commandLine, workingDirectory) {
  // Someone tried to run a second instance, we should focus our window.
  if (mainWindow) {
    if (mainWindow.isMinimized()) {
      mainWindow.restore()
    }
    mainWindow.focus()
  }
})

if (shouldQuit) {
  app.quit()
}

// keep window in osx
global.isMac = process.platform === 'darwin'
if (!global.isMac) {
  app.on('browser-window-created', function (event, browserWindow) {
    browserWindow.setMenu(null)
  })
}

// Load app config from gbf-config.json
gbfConfig.load()
global.gbfConfig = gbfConfig

const tracking = function () {
  let chromeId = gbfConfig.get('chromeId')
  if (chromeId) {
    gbfConfig.get('suchTracking')
  } else {
    chromeId = getRandomToken()
    gbfConfig.set('chromeId', chromeId)
  }
}

const getConfig = function () {
  mainWindow = createWin.call(this, {
    width: gbfConfig.get('width'),
    height: gbfConfig.get('height')
  })
}

const createWin = function (size) {
  const windowWidth = size.width || 400
  const windowHeight = size.height || 570
  windowBounds = { width: windowWidth, height: windowHeight }
  const win = new BrowserWindow({
    minWidth: 400,
    minHeight: 570,
    maxWidth: 784,
    width: windowWidth,
    height: windowHeight,
    useContentSize: true,
    fullscreen: false,
    resizable: true
  })
  win.loadURL('file://' + require('path').join(__dirname, '/../index.html'))
  win.on('close', onClose)
  win.on('closed', onClosed)

  return win
}

app.on('ready', function () {
  global.appVersionName = shellAppSDKMain.getAppVersionName() || '9.9.9'

  shellAppSDKMain.init(
    {
      mobageAppId: '12016007',
      sessionPartition: 'persist:granbluefantasyusers',
      onBeforeSendHeaders: function (a) {
        a.requestHeaders['X-ChromeApp-TID'] = gbfConfig.get('chromeId')
        a.requestHeaders['X-AndApp'] = '1'
        a.requestHeaders['X-AndApp-Version'] = global.appVersionName
      },
      locationValidatorParams: {
        additionalWhiteList: WhiteList,
        onNavigatedToInvalidLocation: function (a, b, c) {
          shell.openExternal(c)
        }
      }
    },
    function (error) {
      if (error) {
        const errorMessage = getErrorMessageForDisplay(error)
        if (errorMessage) {
          displayErrorAndQuit(errorMessage)
        } else {
          process.nextTick(function () {
            app.quit()
          })
        }
        return
      }
      if (global.isMac === true) {
        var appMenu = Menu.buildFromTemplate([
          {
            label: '',
            submenu: [
              { role: 'hide' },
              { type: 'separator' },
              { role: 'quit' }
            ]
          },
          { label: 'File', submenu: [{ role: 'close' }] },
          {
            label: 'Edit',
            submenu: [
              { role: 'undo' },
              { role: 'redo' },
              { type: 'separator' },
              { role: 'cut' },
              { role: 'copy' },
              { role: 'paste' },
              { role: 'delete' },
              { role: 'selectall' },
              { type: 'separator' }
            ]
          },
          {
            label: 'Window',
            submenu: [{ role: 'minimize' }, { role: 'front' }]
          }
        ])
        Menu.setApplicationMenu(appMenu)
      }
      gbfConfig.loaded.then(function () {
        tracking.call()
        getConfig.call()
      })
      app.on('window-all-closed', function () {
        process.platform !== 'darwin' && app.quit()
      })
    }
  )
})

ipcMain.on('appUpdate', function () {
  shellAppSDKMain.openAppDetailPage(function () {
    app.quit()
  })
})
// keep bounds to save to config
ipcMain.on('onBoundsChanged', function (event, args) {
  windowBounds = args
})

// handle common error
process.on('unhandledRejection', function (error) {})
process.on('uncaughtException', function (error) {})
